function fbmRLcovar(H,n,T)
% Save eignevalue decompositin of covariance matrix for [WH, W], 
% Hurst param H, on [0,T], for time points [0=t1, ...,tn+1=T].
% Here we use the covar for the Riemann-Liouville fBM.
disp('Function fbm starting...')
tic
t = T/n:T/n:T;
S11 = nan(n, n);
S12 = nan(n, n); 
S22 = nan(n, n);
gam = 0.5 - H;
% require function pfq from Mike Sheppard MIT Lincoln Laboratory
G = @(x) 2.0*H*( x.^(-gam)/(1.0-gam) + (gam*x.^(-1.0-gam)./(1.0-gam)) .* ...
    pfq([1.0, 1.0+gam], 3.0-gam, x.^(-1.0))/(2.0-gam) );
disp('Computing blocks S11 S12 S22')
[X,Y] = meshgrid(t,t);
Gmat = G((tril(Y./X)+tril(Y./X)') - eye(size(Y,1)).*diag(Y./X));
Gmat = Gmat.*~eye(size(Gmat)) + eye(size(Gmat)); % diag is 1 b/c G(1)=1 
S11 = ((tril(X)+tril(X)') - eye(size(X,1)).*diag(X)).^(2*H) .* Gmat;
S12 = sqrt(2*H)*(Y.^(H+0.5)-(Y-min(Y,X)).^(H+0.5))./(H+0.5);
S22 = min(X,Y);
%
% S11b = nan(n, n);
% S12b = nan(n, n); 
% S22b = nan(n, n);
% for i=1:n % compute covariance blocks
%     for j=1:n
%        ti=t(i); tj=t(j);
%        % S11 is cov(WH_ti, WH_tj) for RL fBM
%        if i == j % then  ti = tj
%            S11b(i,i) = ti^(2*H);
%        elseif j > i % then tj > ti
%            S11b(i,j) = ti^(2*H) * G(tj/ti);
%        else % then ti > tj
%            S11b(i,j) = S11(j,i);
%        end
%        % S12 is cov(WH_ti, W_tj)
%        S12b(i,j) = sqrt(2*H)*(ti^(H+0.5)-(ti-min(ti,tj))^(H+0.5))/(H+0.5);
%        % S22 is cov(W_ti, W_tj)
%        S22b(i,j) = 0.5*(ti+tj-abs(ti-tj)); % OR min(ti,tj);
%     end 
% end
toc
disp('Finished blocks S11 S12 S22')
% LDL decomposition; defualts to Cholesky method
disp('Computing L D via eig')
% Sig = [S11 S12; S12.' S22];
% cond(Sig)
[L,D] = eig([S11 S12; S12.' S22]);
% op = Sig * L - Sig * L * D;
% [L,D] = eig([S11 S12; S12.' S22], [S11 S12; S12.' S22], 'qz');
% op1 = Sig * L1 - Sig * L1 * D1;
% SAVING
satxt = sprintf('covar/RLfbm_LD_H=%0.2f_logn=%i_T=%1.1f.mat',H,log2(n),T);
save(satxt, 'L', 'D');
fprintf('L D saved to %s\n',satxt)
end
