function gendata2(H,n,T,bigM)
% Generate path (WH, W) using a eig decomposition (cf. fbmRLcovar.m)
% for bigM samples in batches for Hurst H at time points 0=t1, ...,tn+1=T;
% Calculate and save XTref and XTdt final time of underlying reference and Euler, resp.
    t = 0:T/n:T;
    gap=0;
	numDt = log2(n)-gap;
	dt = T/n * 2.^(gap:numDt+gap-1);
    M = 1e5;
    %
    load(sprintf('covar/RLfbm_LD_H=%0.2f_logn=%i_T=%1.1f.mat',H,log2(n),T));
    %
    XTref = nan(1,bigM);
    XTdt = nan(numDt,bigM);
    for i= 1:bigM/M
    	fprintf('Starting batch %i of %i\n',i, bigM/M)
    	fprintf('... generating (WH,W) for H=%0.2f on log2(n)=%i in batches log10(M)=%i\n', H, log2(n), log10(M))
        z = randn(2*n,M);
        X = real(L*(D^0.5)*z);
        WH = [zeros(1,M); X(1:n, :)];
        W = [zeros(1,M); X(n+1:end, :)];
		fprintf('... calculating XTref for H=%0.2f\n', H)
        XTref(1,(i-1)*M+1:i*M) = sum(WH(1:end-1,:) .* diff(W(1:end,:)));
        fprintf('... calculating XTdt for H=%0.2f\n', H)
        for j=1:numDt
            XTdt(j,(i-1)*M+1:i*M) =  sum(WH(1:2^(j+gap-1):end-1,:) .* diff(W(1:2^(j+gap-1):end,:)));
        end
        clear WH W X z
    end
    satxt = sprintf('data/XTdat_H=%0.2f_logn=%i_bigM=%1.0e_NoGap.mat',H,log2(n),bigM);
    save(satxt, 'H', 'XTref', 'n', 'XTdt', 'dt')
    fprintf('... ... file %s saved\n', satxt)
end
